//toggle transparent overlay of crosses marking positions of copper mines
function showMines(minetype)
{
	if (document.getElementById("cbMinesCopper").checked)
		document.getElementById("mapMinesCopper").style.display = "block";
	else
		document.getElementById("mapMinesCopper").style.display = "none";
		
	if (document.getElementById("cbMinesZinc").checked)
		document.getElementById("mapMinesZinc").style.display = "block";
	else
		document.getElementById("mapMinesZinc").style.display = "none";
		
	if (document.getElementById("cbMinesLead").checked)
		document.getElementById("mapMinesLead").style.display = "block";
	else
		document.getElementById("mapMinesLead").style.display = "none";
}

//drop-down to choose different maps
function changeMap (mapNum)
{
	console.log(mapNum);
	if (mapNum == 0)
		document.getElementById("mapMain").src = "images/ig_sed_met.png";
	if (mapNum == 1)
		document.getElementById("mapMain").src = "images/ig.png";
	if (mapNum == 2)
		document.getElementById("mapMain").src = "images/met.png";
	if (mapNum == 3)
		document.getElementById("mapMain").src = "images/sed.png";
	if (mapNum == 4)
		document.getElementById("mapMain").src = "images/copper_concs.png";
}
