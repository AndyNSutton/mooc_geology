//---------------------------------------------------------------------------

#ifndef GeolMapKeyH
#define GeolMapKeyH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include <Graphics.hpp>
//---------------------------------------------------------------------------
class TFormGeolMapKey : public TForm
{
__published:	// IDE-managed Components
    TImage *Image1;
    void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
private:	// User declarations
public:		// User declarations
    __fastcall TFormGeolMapKey(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TFormGeolMapKey *FormGeolMapKey;
//---------------------------------------------------------------------------
#endif
