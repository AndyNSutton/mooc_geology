//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "Main.h"
#include "UAboutBox.h"
#include "GeolMapKey.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "oBitBtn"
#pragma link "oBitMapTrackBar"
#pragma link "oCustomButton"
#pragma link "oDoubleBufferedPaintBox"
#pragma resource "*.dfm"
TFormMain *FormMain;
//---------------------------------------------------------------------------
__fastcall TFormMain::TFormMain(TComponent* Owner)
    : TForm(Owner)
{
    
}
//---------------------------------------------------------------------------
void __fastcall TFormMain::FormCreate(TObject *Sender)
{
    MapImage = new Graphics::TBitmap;
    KeyImage = new Graphics::TBitmap;
    Maposb1 = new Graphics::TBitmap;
    Maposb2 = new Graphics::TBitmap;

    //for some reason maximised form leaves a few pixels gap at the top otherwise
    FormMain->Left = 0;
    FormMain->Top = 0;

    //LoadMap(ExtractFilePath(Application->ExeName) + "\maps\\Cu_map.bmp", true);
    CheckDesktopSize();
    dbpbMap2->Left = dbpbMap1->Width + 15;

    cbMap1->ItemIndex = 0;
    cbMap2->ItemIndex = 1;
    cbGeolLayers1->ItemIndex = 0;

    PanelTop1->Top = 5 + ((DesktopSize-1) * 7);
    PanelTop2->Top = 5 + ((DesktopSize-1) * 7);
    
    dbpbMap1->Top = PanelTop1->Top + PanelTop1->Height + 5 + ((DesktopSize-1) * 7);
    dbpbMap2->Top = dbpbMap1->Top;

    Panel1->Top = dbpbMap1->Top + dbpbMap1->Height + 5 + ((DesktopSize-1) * 7);
    Panel2->Top = Panel1->Top;

    Panel1->Left = dbpbMap1->Left;
    Panel2->Left = dbpbMap2->Left;
    PanelTop1->Left = dbpbMap1->Left;
    PanelTop2->Left = dbpbMap2->Left;

    grid = "_grid_"; grid1 = grid; grid2 = grid;
    boundaries = ""; boundaries1 = boundaries; boundaries2 = boundaries;
    cbMap1Change(cbMap1); //to load initial maps
    cbMap1Change(cbMap2);

}
//---------------------------------------------------------------------------
void TFormMain::CheckDesktopSize()
{
    //find out how many pixels across the available desktop is so that we can
    //display appropriately sized maps and layout interface accordingly
    
    if (Screen->DesktopWidth <=800)
        DesktopSize = 1;
    if (Screen->DesktopWidth >800 && Screen->DesktopWidth <=1024)
        DesktopSize = 2;
    if (Screen->DesktopWidth >1024)
        DesktopSize = 3;

    
    switch (DesktopSize)
    {
        case 1: size = "small"; break;
        case 2: size = "med"; break;
        case 3: size = "large"; break;
    }

    switch (DesktopSize)
    {
      case 1:
        dbpbMap1->Width = 390;
        dbpbMap1->Height = 386;
        dbpbMap2->Width = 390;
        dbpbMap2->Height = 386;
      break;
      case 2:
        dbpbMap1->Width = 500;
        dbpbMap1->Height = 494;
        dbpbMap2->Width = 500;
        dbpbMap2->Height = 494;
      break;
      case 3:
        dbpbMap1->Width = 560;
        dbpbMap1->Height = 554;
        dbpbMap2->Width = 560;
        dbpbMap2->Height = 554;
      break;
    }

}
//---------------------------------------------------------------------------
void TFormMain::LoadMap(AnsiString filename, int dbpb, bool Offscreen)
{
    if (Offscreen)
    {
        if (dbpb == 1)
            Maposb1->LoadFromFile(filename); //load to offscreen buffer
        else
            Maposb2->LoadFromFile(filename); //load to offscreen buffer
    }
    else
    {
        MapImage->LoadFromFile(filename);       //load to screen

         //Peter wants a border
         MapImage->Canvas->LineTo(dbpbMap1->Width-1, 0);
         MapImage->Canvas->LineTo(dbpbMap1->Width-1, dbpbMap1->Height-1);
         MapImage->Canvas->LineTo(0, dbpbMap1->Height-1);
         MapImage->Canvas->LineTo(0, 0);

        if (dbpb == 1)
        {
            dbpbMap1->Canvas->Draw(0,0, MapImage);


            dbpbMap1->Save();
            dbpbMap1->Show();
            dbpbMap1->Visualize();
        }
        else
        {
            dbpbMap2->Canvas->Draw(0,0, MapImage);
            dbpbMap2->Save();
            dbpbMap2->Show();
            dbpbMap2->Visualize();
        }
    }
}
//---------------------------------------------------------------------------
void TFormMain::LoadKey(AnsiString filename, int dbpb)
{
    KeyImage->LoadFromFile(filename);
    if (dbpb == 1)
        {
            dbpbKey1->Canvas->Draw(0,0, KeyImage);
            dbpbKey1->Save();
            if (CheckBoxKey1->Checked)
            {
                dbpbKey1->Show();
                dbpbKey1->Visualize();
            }
        }
        else
        {
            dbpbKey2->Canvas->Draw(0,0, KeyImage);
            dbpbKey2->Save();
            if (CheckBoxKey2->Checked)
            {
                dbpbKey2->Show();
                dbpbKey2->Visualize();
            }
        }
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::About1Click(TObject *Sender)
{
    AboutBox->Show();    
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::dbpbMap1MouseMove(TObject *Sender,
      TShiftState Shift, int X, int Y)
{
    //create a variable for the 2 controls so that I don't have to replicate code
    oDoubleBufferedPaintBox *dbpbName = dynamic_cast < oDoubleBufferedPaintBox *>(Sender);

    if (Shift.Contains(ssLeft)||Shift.Contains(ssRight)) //doesn't count if they're dragging with mouse down
        return;


    bool boolTooltipVisible;

    //get colour under mouse on osbitmap
    TColor intOSB;
    if (dbpbName == dbpbMap1)
        intOSB = Maposb1->Canvas->Pixels[X][Y];
    else
        intOSB = Maposb2->Canvas->Pixels[X][Y];

    //FormMain->Caption = (AnsiString)intOSB;
    AnsiString Tooltip;

    dbpbName->Cursor = crCross;
    int index;
    if (dbpbName == dbpbMap1)
        index = cbMap1->ItemIndex;
    else
        index = cbMap2->ItemIndex;
    switch (index)
    {
    //geology
    case 0:
        switch (intOSB)
        {
            case 65433: Tooltip = "Tertiary sedimentary rocks";  break;
            case 16776960: Tooltip = "Carboniferous sedimentary rocks";  break;
            case 3355545: Tooltip = "Devonian sedimentary rocks";  break;
            case 16751103: Tooltip = "Silurian sedimentary rocks";  break;
            case 12322053: Tooltip = "Ordovician sedimentary rocks";  break;
            case 13434879: Tooltip = "Cambrian sedimentary rocks";  break;
            case 3394764: Tooltip = "Precambrian metamorphic rocks";  break;
            case 6750207: Tooltip = "Ordovician rhyolitic lavas & tuffs"; break;
            case 3394815: Tooltip = "Ordovician basaltic lavas"; break;
            case 10079487: Tooltip = "Precambrian rhyolitic lavas & tuffs"; break;
            case 5936202: Tooltip = "Dolerite"; break;
            case 255: Tooltip = "Granite & diorite"; break;

            default: dbpbName->Cursor = crDefault; break;
        }
    break;
    //copper
    case 1:
        switch (intOSB)
        {
            case 5570560: Tooltip = "<15 ppm";  break;
            case 14483456: Tooltip = "15-17 ppm";  break;
            case 16724787: Tooltip = "17-19 ppm";  break;
            case 16764006: Tooltip = "19-21 ppm";  break;
            case 3394713: Tooltip = "21-27 ppm";  break;
            case 13434879: Tooltip = "27-37 ppm";  break;
            case 39423: Tooltip = "37-64 ppm";  break;
            case 13311: Tooltip = "64-116 ppm"; break;
            case 204: Tooltip = "116-457 ppm"; break;
            case 119: Tooltip = "> 457 ppm"; break;

            default: dbpbName->Cursor = crDefault; break;
        }
    break;
    //Mo
    case 2:
        switch (intOSB)
        {
            case 5570560: Tooltip = "<0.70 ppm";  break;
            case 14483456: Tooltip = "0.70-0.81 ppm";  break;
            case 16724787: Tooltip = "0.81-0.90 ppm";  break;
            case 16764006: Tooltip = "0.90-1.05 ppm";  break;
            case 3394713: Tooltip = "1.05-1.44 ppm";  break;
            case 13434879: Tooltip = "1.44-2.01 ppm";  break;
            case 39423: Tooltip = "2.01-2.90 ppm";  break;
            case 13311: Tooltip = "2.90-4.37 ppm"; break;
            case 204: Tooltip = "4.37-10.55 ppm"; break;
            case 119: Tooltip = "> 10.55 ppm"; break;

            default: dbpbName->Cursor = crDefault; break;
        }
    break;
    //Pb
    case 3:
        switch (intOSB)
        {
            case 5570560: Tooltip = "<25 ppm";  break;
            case 14483456: Tooltip = "25-29 ppm";  break;
            case 16724787: Tooltip = "29-32 ppm";  break;
            case 16764006: Tooltip = "32-37 ppm";  break;
            case 3394713: Tooltip = "37-51 ppm";  break;
            case 13434879: Tooltip = "51-79 ppm";  break;
            case 39423: Tooltip = "79-183 ppm";  break;
            case 13311: Tooltip = "183-344 ppm"; break;
            case 204: Tooltip = "344-2222 ppm"; break;
            case 119: Tooltip = "> 2222 ppm"; break;

            default: dbpbName->Cursor = crDefault; break;
        }
    break;
    //Zn
    case 4:
        switch (intOSB)
        {
            case 5570560: Tooltip = "<101 ppm";  break;
            case 14483456: Tooltip = "102-117 ppm";  break;
            case 16724787: Tooltip = "117-127 ppm";  break;
            case 16764006: Tooltip = "127-147 ppm";  break;
            case 3394713: Tooltip = "147-199 ppm";  break;
            case 13434879: Tooltip = "199-291 ppm";  break;
            case 39423: Tooltip = "291-513 ppm";  break;
            case 13311: Tooltip = "513-752 ppm"; break;
            case 204: Tooltip = "752-1977 ppm"; break;
            case 119: Tooltip = "> 1977 ppm"; break;

            default: dbpbName->Cursor = crDefault; break;
        }
    break;
    }

        boolTooltipVisible = 1;

    //draw the text showing which hotspot we're over in a box
    dbpbName->ReloadSaved();
    dbpbName->Canvas->Font->Color = clBlack;
    dbpbName->Canvas->Font->Size = 10;
    if (Tooltip != "" && boolTooltipVisible)
    {
        dbpbName->Canvas->Brush->Color = TColor(0x00C2FEFD);
        int XCursor;  int YCursor;

        //adjust if it's going to go off edge of canvas
        if (X + dbpbName->Canvas->TextWidth(Tooltip) + 20 > dbpbName->Width)
            XCursor = X - dbpbName->Canvas->TextWidth(Tooltip);
        else
            XCursor = X;

        //adjust if it's going to go off bottom of canvas
        if (Y + dbpbName->Canvas->TextHeight(Tooltip) + 95 > dbpbName->Height)
            YCursor = Y - dbpbName->Canvas->TextHeight(Tooltip) - 15;
        else
            YCursor = Y;

        dbpbName->Canvas->Rectangle (XCursor + 14, YCursor + 14, XCursor + dbpbName->Canvas->TextWidth(Tooltip) + 23, YCursor + 35);
        dbpbName->Canvas->TextOut(XCursor + 18, YCursor + 16, Tooltip);
    }
    dbpbName->Visualize();
    Application->ProcessMessages();
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::Exit1Click(TObject *Sender)
{
    Application->Terminate();    
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::cbMap1Change(TObject *Sender)
{
//create a variable for the 2 controls so that I don't have to replicate code
    TComboBox *senderName = dynamic_cast < TComboBox *>(Sender);
    int index;
    if (senderName == cbMap1)
    {
        index = 1;
        grid = grid1;
        boundaries = boundaries1;
    }
    else
    {
        index = 2;
        grid = grid2;
        boundaries = boundaries2;
    }

    if (index == 1)
    {
        if (cbMap1->ItemIndex == 0)
        {
            LabelRockUnits1->Show();
            cbGeolLayers1->Show();
            dbpbKey1->Hide();
            cbGeolLayers1->ItemIndex = 0;
            CheckBoxGB1->Hide();
            if (FormGeolMapKey)
            {
                if (cbMap2->ItemIndex == 0 && CheckBoxKey2->Checked)
                    FormGeolMapKey->Visible = true;
                else
                    FormGeolMapKey->Visible = CheckBoxKey1->Checked;
            }
        }
        else
        {
            LabelRockUnits1->Hide();
            cbGeolLayers1->Hide();
            CheckBoxGB1->Show();
        }
    }
    else
    {
        if (cbMap2->ItemIndex == 0)
        {
            LabelRockUnits2->Show();
            cbGeolLayers2->Show();
            dbpbKey2->Hide();
            cbGeolLayers2->ItemIndex = 0;
            CheckBoxGB2->Hide();
            if (FormGeolMapKey)
                if (cbMap1->ItemIndex == 0 && CheckBoxKey1->Checked)
                    FormGeolMapKey->Visible = true;
                else
                    FormGeolMapKey->Visible = CheckBoxKey2->Checked;
            }
        else
        {
            LabelRockUnits2->Hide();
            cbGeolLayers2->Hide();
            CheckBoxGB2->Show();
        }

    }
    //can get confused if you select a new map while geol key is displaying so hide if neither map is the geol one
    if (FormGeolMapKey && cbMap1->ItemIndex != 0 && cbMap2->ItemIndex != 0)
        FormGeolMapKey->Hide();
    //more circumstances when I want to hide the geol key; all getting a bit messy!
    if (FormGeolMapKey && cbMap2->ItemIndex != 0 && CheckBoxKey1->Checked == false)
        FormGeolMapKey->Hide();
    if (FormGeolMapKey && cbMap1->ItemIndex != 0 && CheckBoxKey2->Checked == false)
        FormGeolMapKey->Hide();


        switch (senderName->ItemIndex)
        {
        case 0:
            LoadMap(ExtractFilePath(Application->ExeName) + "\maps\\geol"+ grid + size+".bmp", index, false);
            LoadMap(ExtractFilePath(Application->ExeName) + "\maps\\geol"+ grid + size+".bmp", index, true);

            break;
        case 1:
            LoadMap(ExtractFilePath(Application->ExeName) + "\maps\\Cu"+ grid + boundaries + size+".bmp", index, false);
            LoadMap(ExtractFilePath(Application->ExeName) + "\maps\\Cu"+ grid + boundaries + size+".bmp", index, true);
            LoadKey(ExtractFilePath(Application->ExeName) + "\maps\\key_Cu.bmp", index);
            break;
        case 2:
            LoadMap(ExtractFilePath(Application->ExeName) + "\maps\\Mo"+ grid + boundaries + size+".bmp", index, false);
            LoadMap(ExtractFilePath(Application->ExeName) + "\maps\\Mo"+ grid + boundaries + size+".bmp", index, true);
            LoadKey(ExtractFilePath(Application->ExeName) + "\maps\\key_Mo.bmp", index);
            break;
        case 3:
            LoadMap(ExtractFilePath(Application->ExeName) + "\maps\\Pb"+ grid + boundaries + size+".bmp", index, false);
            LoadMap(ExtractFilePath(Application->ExeName) + "\maps\\Pb"+ grid + boundaries + size+".bmp", index, true);
            LoadKey(ExtractFilePath(Application->ExeName) + "\maps\\key_Pb.bmp", index);
            break;
        case 4:
            LoadMap(ExtractFilePath(Application->ExeName) + "\maps\\Zn"+ grid + boundaries + size+".bmp", index, false);
            LoadMap(ExtractFilePath(Application->ExeName) + "\maps\\Zn"+ grid + boundaries + size+".bmp", index, true);
            LoadKey(ExtractFilePath(Application->ExeName) + "\maps\\key_Zn.bmp", index);
            break;
        }
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::CheckBoxGrid1Click(TObject *Sender)
{
    if (CheckBoxGrid1->Checked)
        grid1 = "_grid_";
    else
        grid1 = "_";

    if (cbMap1->ItemIndex == 0)
        cbGeolLayers1Change(cbGeolLayers1); //send event as if it's come from this combobox not here!
    else
        cbMap1Change(cbMap1);
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::CheckBoxGrid2Click(TObject *Sender)
{
    if (CheckBoxGrid2->Checked)
        grid2 = "_grid_";
    else
        grid2 = "_";

    if (cbMap2->ItemIndex == 0)
        cbGeolLayers1Change(cbGeolLayers2); //send event as if it's come from this combobox not here!
    else
        cbMap1Change(cbMap2);
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::CheckBoxKey1Click(TObject *Sender)
{
    if (cbMap1->ItemIndex == 0)
    {
        if (cbMap2->ItemIndex == 0 && CheckBoxKey2->Checked)
            FormGeolMapKey->Visible = true;
        else
            FormGeolMapKey->Visible = CheckBoxKey1->Checked;
    }
    else
    {
        dbpbKey1->Visible = CheckBoxKey1->Checked;
    }
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::CheckBoxKey2Click(TObject *Sender)
{
    if (cbMap2->ItemIndex == 0)
    {
        if (cbMap1->ItemIndex == 0 && CheckBoxKey1->Checked)
            FormGeolMapKey->Visible = true;
        else
            FormGeolMapKey->Visible = CheckBoxKey2->Checked;
    }
    else
    {
        dbpbKey2->Visible = CheckBoxKey2->Checked;
    }
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::cbGeolLayers1Change(TObject *Sender)
{
    //show the map with the selected individual layer

    //create a variable for the 2 controls so that I don't have to replicate code
    TComboBox *senderName = dynamic_cast < TComboBox *>(Sender);
    int index;
    if (senderName == cbGeolLayers1)
    {
        index = 1;
        grid = grid1;
    }
    else
    {
        index = 2;
        grid = grid2;
    }

    switch (senderName->ItemIndex)
    {
      case 0: LoadMap(ExtractFilePath(Application->ExeName) + "\maps\\geol"+ grid + size+".bmp", index, false); break;
      case 1: LoadMap(ExtractFilePath(Application->ExeName) + "\maps\\tertiary"+ grid + size+".bmp", index, false); break;
      case 2: LoadMap(ExtractFilePath(Application->ExeName) + "\maps\\carboniferous"+ grid + size+".bmp", index, false); break;
      case 3: LoadMap(ExtractFilePath(Application->ExeName) + "\maps\\devonian"+ grid + size+".bmp", index, false); break;
      case 4: LoadMap(ExtractFilePath(Application->ExeName) + "\maps\\silurian"+ grid + size+".bmp", index, false); break;
      case 5: LoadMap(ExtractFilePath(Application->ExeName) + "\maps\\ordovician"+ grid + size+".bmp", index, false); break;
      case 6: LoadMap(ExtractFilePath(Application->ExeName) + "\maps\\cambrian"+ grid + size+".bmp", index, false); break;
      case 7: LoadMap(ExtractFilePath(Application->ExeName) + "\maps\\mona"+ grid + size+".bmp", index, false); break;
      case 8: LoadMap(ExtractFilePath(Application->ExeName) + "\maps\\acidlavasord"+ grid + size+".bmp", index, false); break;
      case 9: LoadMap(ExtractFilePath(Application->ExeName) + "\maps\\basiclava"+ grid + size+".bmp", index, false); break;
      case 10: LoadMap(ExtractFilePath(Application->ExeName) + "\maps\\arfonian"+ grid + size+".bmp", index, false); break;
      case 11: LoadMap(ExtractFilePath(Application->ExeName) + "\maps\\dolerite"+ grid + size+".bmp", index, false); break;
      case 12: LoadMap(ExtractFilePath(Application->ExeName) + "\maps\\granite"+ grid + size+".bmp", index, false); break;
    }
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::CheckBoxGB1Click(TObject *Sender)
{
    if (CheckBoxGB1->Checked)
        boundaries1 = "boundaries_";
    else
        boundaries1 = "";

    if (cbMap1->ItemIndex == 0)
        cbGeolLayers1Change(cbGeolLayers1); //send event as if it's come from this combobox not here!
    else
        cbMap1Change(cbMap1);
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::CheckBoxGB2Click(TObject *Sender)
{
    if (CheckBoxGB2->Checked)
        boundaries2 = "boundaries_";
    else
        boundaries2 = "";

    if (cbMap2->ItemIndex == 0)
        cbGeolLayers1Change(cbGeolLayers2); //send event as if it's come from this combobox not here!
    else
        cbMap1Change(cbMap2);
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::PrintKeyClick(TObject *Sender)
{
    //if (PrintDialog1->Execute())
        //FormGeolMapKey->Print();
        PrintForm(FormGeolMapKey, poLandscape);
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::Printmaps1Click(TObject *Sender)
{
    //if (PrintDialog1->Execute())
        PrintForm(FormMain, poLandscape);
}
//---------------------------------------------------------------------------
void TFormMain::PrintForm(TForm * form, TPrinterOrientation orientation)
{
   Graphics::TBitmap * bmp = new Graphics::TBitmap();
   bmp->Width = form->ClientWidth;
   bmp->Height = form->ClientHeight;
   bmp->Canvas->Brush = form->Brush;
   bmp->Canvas->FillRect(form->ClientRect);
   bmp->Canvas->Lock();

   //copy the pixels of the form to the temporary bitmap just created
   BitBlt(bmp->Canvas->Handle, 0, 0, bmp->Width, bmp->Height,
      GetDC(form->Handle), 0, 0, SRCCOPY);
   bmp->Canvas->Unlock();

   TPrinter * prn = Printer();
   prn->Orientation = orientation;
   prn->BeginDoc();

   //rescale to fit bitmap to size of page but preserving aspect ratio
   HDC hDC = prn->Canvas->Handle;
   SetMapMode(hDC, MM_ISOTROPIC);
   int x = GetDeviceCaps(hDC, PHYSICALOFFSETX);
   int y = GetDeviceCaps(hDC, PHYSICALOFFSETY);
   int w = GetDeviceCaps(hDC, PHYSICALWIDTH) - x*4;
   int h = GetDeviceCaps(hDC, PHYSICALHEIGHT) - y*4;
   SetViewportOrgEx(hDC, x, y, NULL);
   SetViewportExtEx(hDC, w, h, NULL);
   SetWindowExtEx(hDC, bmp->Width, bmp->Height, NULL);

   //output the bitmap to the printer device
   prn->Canvas->Draw(0,0, bmp);

   prn->EndDoc();

   delete bmp;
}

