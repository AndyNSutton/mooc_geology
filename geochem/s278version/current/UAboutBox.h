//---------------------------------------------------------------------------
#ifndef UAboutBoxH
#define UAboutBoxH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "oBitBtn.h"
#include "oCustomButton.h"
#include <ExtCtrls.hpp>
#include <ComCtrls.hpp>
#include <Graphics.hpp>
#include <ImgList.hpp>
//---------------------------------------------------------------------------
class TAboutBox : public TForm
{
__published:	// IDE-managed Components
    oBitBtn *btnOK;
    TImageList *imglSmallButton;
    oBitBtn *btnCredits;
    oBitBtn *btnContacts;
    oBitBtn *btnCopyright;
    TImageList *imglLargeButton;
    TNotebook *Notebook;
    TImage *imgOULogo;
    TLabel *lblTitle;
    TLabel *lblVersion;
    TLabel *lblCopyright;
    TLabel *lblRights;
    TRichEdit *rtfContacts;
    TRichEdit *rtfCredits;
    TImage *Image1;
    TImageList *ImageListGeneric;
    void __fastcall btnOKClick(TObject *Sender);
    void __fastcall btnPageClick(TObject *Sender);
    
    
    
private:	// User declarations
public:		// User declarations
    enum { mrCopyright = 100, mrAcknowledgements, mrContact };
    __fastcall TAboutBox(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TAboutBox *AboutBox;
//---------------------------------------------------------------------------
#endif
