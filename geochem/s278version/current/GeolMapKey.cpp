//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "GeolMapKey.h"
#include "Main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFormGeolMapKey *FormGeolMapKey;
//---------------------------------------------------------------------------
__fastcall TFormGeolMapKey::TFormGeolMapKey(TComponent* Owner)
    : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TFormGeolMapKey::FormClose(TObject *Sender,
      TCloseAction &Action)
{
    if (FormMain->cbMap1->ItemIndex == 0)
        FormMain->CheckBoxKey1->Checked = false;

    if (FormMain->cbMap2->ItemIndex == 0)
        FormMain->CheckBoxKey2->Checked = false;
}
//---------------------------------------------------------------------------
