object FormMain: TFormMain
  Left = 93
  Top = 138
  Width = 809
  Height = 569
  Caption = 'Geochemical exploration in North Wales'
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object dbpbMap1: oDoubleBufferedPaintBox
    Left = 7
    Top = 41
    Width = 390
    Height = 385
    Cursor = crCross
    OnMouseMove = dbpbMap1MouseMove
  end
  object dbpbMap2: oDoubleBufferedPaintBox
    Left = 405
    Top = 41
    Width = 390
    Height = 385
    Cursor = crCross
    OnMouseMove = dbpbMap1MouseMove
  end
  object Panel1: TPanel
    Left = 8
    Top = 426
    Width = 385
    Height = 73
    BevelOuter = bvNone
    Color = clWhite
    TabOrder = 0
    object dbpbKey1: oDoubleBufferedPaintBox
      Left = -1
      Top = 34
      Width = 387
      Height = 39
      Visible = False
    end
    object LabelRockUnits1: TLabel
      Left = 118
      Top = 9
      Width = 54
      Height = 13
      Caption = 'Rock units:'
    end
    object CheckBoxGrid1: TCheckBox
      Left = -1
      Top = 8
      Width = 49
      Height = 17
      Caption = 'Grid'
      Checked = True
      State = cbChecked
      TabOrder = 0
      OnClick = CheckBoxGrid1Click
    end
    object CheckBoxKey1: TCheckBox
      Left = 63
      Top = 8
      Width = 49
      Height = 17
      Caption = 'Key'
      TabOrder = 1
      OnClick = CheckBoxKey1Click
    end
    object cbGeolLayers1: TComboBox
      Left = 179
      Top = 6
      Width = 193
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 2
      OnChange = cbGeolLayers1Change
      Items.Strings = (
        'All'
        'Tertiary sedimentary'
        'Carboniferous sedimentary'
        'Devonian sedimentary'
        'Silurian sedimentary'
        'Ordovician sedimentary'
        'Cambrian sedimentary'
        'Precambrian metamorphic'
        'Ordovician rhyolitic lavas & tuffs'
        'Ordovician basaltic lavas'
        'Precambrian lavas & tuffs'
        'Dolerite'
        'Granite & diorite')
    end
    object CheckBoxGB1: TCheckBox
      Left = 117
      Top = 8
      Width = 154
      Height = 17
      Caption = 'Geological boundaries'
      TabOrder = 3
      Visible = False
      OnClick = CheckBoxGB1Click
    end
  end
  object Panel2: TPanel
    Left = 402
    Top = 426
    Width = 393
    Height = 73
    BevelOuter = bvNone
    Color = clWhite
    TabOrder = 1
    object dbpbKey2: oDoubleBufferedPaintBox
      Left = 2
      Top = 34
      Width = 387
      Height = 39
    end
    object LabelRockUnits2: TLabel
      Left = 118
      Top = 9
      Width = 54
      Height = 13
      Caption = 'Rock units:'
      Visible = False
    end
    object CheckBoxGrid2: TCheckBox
      Left = 2
      Top = 8
      Width = 49
      Height = 17
      Caption = 'Grid'
      Checked = True
      State = cbChecked
      TabOrder = 0
      OnClick = CheckBoxGrid2Click
    end
    object CheckBoxKey2: TCheckBox
      Left = 66
      Top = 8
      Width = 49
      Height = 17
      Caption = 'Key'
      Checked = True
      State = cbChecked
      TabOrder = 1
      OnClick = CheckBoxKey2Click
    end
    object cbGeolLayers2: TComboBox
      Left = 179
      Top = 6
      Width = 193
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 2
      Visible = False
      OnChange = cbGeolLayers1Change
      Items.Strings = (
        'All'
        'Tertiary sedimentary'
        'Carboniferous sedimentary'
        'Devonian sedimentary'
        'Silurian sedimentary'
        'Ordovician sedimentary'
        'Cambrian sedimentary'
        'Precambrian metamorphic'
        'Ordovician rhyolitic lavas & tuffs'
        'Ordovician basaltic lavas'
        'Precambrian lavas & tuffs'
        'Dolerite'
        'Granite & diorite')
    end
    object CheckBoxGB2: TCheckBox
      Left = 117
      Top = 8
      Width = 154
      Height = 17
      Caption = 'Geological boundaries'
      TabOrder = 3
      OnClick = CheckBoxGB2Click
    end
  end
  object PanelTop1: TPanel
    Left = 8
    Top = 3
    Width = 385
    Height = 33
    BevelOuter = bvNone
    Color = clWhite
    TabOrder = 2
    object LabelMap1: TLabel
      Left = 1
      Top = 10
      Width = 65
      Height = 13
      Caption = 'Select a map:'
    end
    object cbMap1: TComboBox
      Left = 77
      Top = 7
      Width = 145
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 0
      OnChange = cbMap1Change
      Items.Strings = (
        'Geology'
        'Cu concentrations'
        'Mo concentrations'
        'Pb concentrations'
        'Zn concentrations')
    end
  end
  object PanelTop2: TPanel
    Left = 390
    Top = 3
    Width = 385
    Height = 33
    BevelOuter = bvNone
    Color = clWhite
    TabOrder = 3
    object LabelMap2: TLabel
      Left = 2
      Top = 6
      Width = 65
      Height = 13
      Caption = 'Select a map:'
    end
    object cbMap2: TComboBox
      Left = 82
      Top = 3
      Width = 145
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 0
      OnChange = cbMap1Change
      Items.Strings = (
        'Geology'
        'Cu concentrations'
        'Mo concentrations'
        'Pb concentrations'
        'Zn concentrations')
    end
  end
  object MainMenu1: TMainMenu
    Left = 352
    Top = 65529
    object File1: TMenuItem
      Caption = '&File'
      object Printmaps1: TMenuItem
        Caption = '&Print maps'
        OnClick = Printmaps1Click
      end
      object PrintKey: TMenuItem
        Caption = 'Print &geological map key'
        OnClick = PrintKeyClick
      end
      object Exit1: TMenuItem
        Caption = 'E&xit'
        OnClick = Exit1Click
      end
    end
    object Help1: TMenuItem
      Caption = '&About'
      object About1: TMenuItem
        Caption = 'A&bout this program'
        OnClick = About1Click
      end
    end
  end
  object PrintDialog1: TPrintDialog
    Left = 320
    Top = 65528
  end
end
