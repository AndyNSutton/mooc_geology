//---------------------------------------------------------------------------

#ifndef MainH
#define MainH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "oBitBtn.h"
#include "oBitMapTrackBar.h"
#include "oCustomButton.h"
#include <ComCtrls.hpp>
#include <ExtCtrls.hpp>
#include <Graphics.hpp>
#include <ImgList.hpp>
#include <Menus.hpp>
#include "oDoubleBufferedPaintBox.h"
#include <MPlayer.hpp>
#include <Dialogs.hpp>
//---------------------------------------------------------------------------
class TFormMain : public TForm
{
__published:	// IDE-managed Components
    TMainMenu *MainMenu1;
    TMenuItem *File1;
    TMenuItem *Exit1;
    TMenuItem *Help1;
    TMenuItem *About1;
    oDoubleBufferedPaintBox *dbpbMap1;
    oDoubleBufferedPaintBox *dbpbMap2;
    TPanel *Panel1;
    TCheckBox *CheckBoxGrid1;
    oDoubleBufferedPaintBox *dbpbKey1;
    TPanel *Panel2;
    TCheckBox *CheckBoxGrid2;
    oDoubleBufferedPaintBox *dbpbKey2;
    TCheckBox *CheckBoxKey1;
    TCheckBox *CheckBoxKey2;
    TComboBox *cbGeolLayers1;
    TLabel *LabelRockUnits1;
    TLabel *LabelRockUnits2;
    TComboBox *cbGeolLayers2;
    TPanel *PanelTop1;
    TLabel *LabelMap1;
    TComboBox *cbMap1;
    TPanel *PanelTop2;
    TLabel *LabelMap2;
    TComboBox *cbMap2;
    TCheckBox *CheckBoxGB1;
    TCheckBox *CheckBoxGB2;
    TMenuItem *PrintKey;
    TMenuItem *Printmaps1;
    TPrintDialog *PrintDialog1;
    void __fastcall FormCreate(TObject *Sender);
    void __fastcall About1Click(TObject *Sender);
    void __fastcall dbpbMap1MouseMove(TObject *Sender, TShiftState Shift,
          int X, int Y);
    void __fastcall Exit1Click(TObject *Sender);
    void __fastcall cbMap1Change(TObject *Sender);
    void __fastcall CheckBoxGrid1Click(TObject *Sender);
    void __fastcall CheckBoxGrid2Click(TObject *Sender);
    void __fastcall CheckBoxKey1Click(TObject *Sender);
    void __fastcall CheckBoxKey2Click(TObject *Sender);
    void __fastcall cbGeolLayers1Change(TObject *Sender);
    void __fastcall CheckBoxGB1Click(TObject *Sender);
    void __fastcall CheckBoxGB2Click(TObject *Sender);
    void __fastcall PrintKeyClick(TObject *Sender);
    void __fastcall Printmaps1Click(TObject *Sender);
private:	// User declarations
public:		// User declarations
    __fastcall TFormMain(TComponent* Owner);

    Graphics::TBitmap * MapImage;
    Graphics::TBitmap * KeyImage;
    Graphics::TBitmap * Maposb1;
    Graphics::TBitmap * Maposb2;
    int Concentration;
    AnsiString size;
    AnsiString grid, grid1, grid2;
    AnsiString boundaries, boundaries1, boundaries2;
    int DesktopSize;
    void LoadMap(AnsiString filename, int dbpb, bool Offscreen);
    void LoadKey(AnsiString filename, int dbpb);
    void CheckDesktopSize();
    void PrintForm(TForm * form, TPrinterOrientation orientation);
};
//---------------------------------------------------------------------------
extern PACKAGE TFormMain *FormMain;
//---------------------------------------------------------------------------
#endif
