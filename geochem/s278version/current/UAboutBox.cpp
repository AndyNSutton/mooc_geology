//About box code lifted from S207 Functions and Derivatives package
//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "UAboutBox.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "oBitBtn"
#pragma link "oCustomButton"
#pragma resource "*.dfm"
TAboutBox *AboutBox;
//---------------------------------------------------------------------------
__fastcall TAboutBox::TAboutBox(TComponent* Owner)
    : TForm(Owner)
{
    //Caption = "About " + Application->Title;
    Notebook->PageIndex = 0;
    rtfCredits->Lines->LoadFromFile ("Credits.rtf");
    rtfContacts->Lines->LoadFromFile ("Contacts.rtf");
}
//---------------------------------------------------------------------------
void __fastcall TAboutBox::btnOKClick(TObject *Sender)
{
    Close();
}

//---------------------------------------------------------------------------
void __fastcall TAboutBox::btnPageClick(TObject *Sender)
{
    TButton* btn = dynamic_cast<TButton*>(Sender);
    if (btn)
        Notebook->PageIndex = btn->Tag;
}
//---------------------------------------------------------------------------











