
var selectedImage, selectedDiv, imageName, timeout, keytimeout;

$(document).ready()
{
	
	$(function() {
		makeAllDraggable();
		
		var $start_counter = $( "#event-start" ),
		  $drag_counter = $( "#event-drag" ),
		  $stop_counter = $( "#event-stop" ),
		  counts = [ 0, 0, 0 ]
		  
			selectedDiv = $("#ruler");
		
		$( ".draggable-ruler" ).draggable({
		  start: function() {
			
			targetparent = event.target.parentElement;
			
			counts[ 0 ]++;
		  },
		  drag: function() {
			counts[ 1 ]++;
			//console.log(counts[1]);
			
		  },
		  stop: function() {
			counts[ 2 ]++;
			
		  },
		  
		});	
		
		/*key events for nudging object around*/
		
		$(document).keyup(function(event){
			nudge(event);
			clearInterval(keytimeout);
			return false;
			console.log("key up");
		});
		
		/*rotation events, continuous while mouse held down*/
		$("#rotateClockwise").mousedown(function(event){
			 timeout = setInterval(function(){
				rotate(5);				
			}, 100);
			return false;			
		});
		
		
		$("#rotateAnticlockwise").mousedown(function(event){
			timeout = setInterval(function(){
				rotate(-5);		
			}, 100);
			return false;			
		});
		
		$("#rotateClockwise").click(function(event){
				rotate(5);							
		});
		$("#rotateAnticlockwise").click(function(event){
				rotate(-5);							
		});
		
		$(document).mouseup(function(){
			clearInterval(timeout);
			return false;
		});
		
		$(document).mousemove(function(ev){
			if(!ev)ev=window.event;
			drawLinesToAxes(ev.clientX,ev.clientY);
			return false;
		});
	});
};

function nudge(evt)
{
	var xPos = parseInt($(selectedDiv).css('left'));
	var yPos = parseInt($(selectedDiv).css('top'));
	
	console.log(evt.keyCode);
	//move selected continent using arrow keys
	//todo - make this work while key is down
	switch (evt.keyCode)
	{
		case 13: selectDivWithEnter(evt.target); break;
		case 37: xPos-=10; break;
		case 38: yPos -= 10; break;
		case 39: xPos += 10; break;
		case 40: yPos += 10; break;
		
		case 65: rotate(-5); break;		
		case 67: rotate(5); break;
	}
	if (evt.keyCode!=13)
	{
		$(selectedDiv).css('left',xPos);
		$(selectedDiv).css('top',yPos);
	}
}

function rotate (amount)
{
	var rotationAngle = getRotationDegrees($(selectedDiv));
				rotationAngle += amount;
				var rotateDeg = "rotate("+rotationAngle+"deg)";
				$(selectedDiv).css({"-webkit-transform": rotateDeg,
									"-moz-transform": rotateDeg,
									"transform": rotateDeg});	
}


function makeAllDraggable()
{
	$("#ruler").draggable();
}


function getRotationDegrees(obj) {
    var matrix = obj.css("-webkit-transform") ||
    obj.css("-moz-transform")    ||
    obj.css("-ms-transform")     ||
    obj.css("-o-transform")      ||
    obj.css("transform");
    if(matrix !== 'none') {
        var values = matrix.split('(')[1].split(')')[0].split(',');
        var a = values[0];
        var b = values[1];
        var angle = Math.round(Math.atan2(b, a) * (180/Math.PI));
    } else { var angle = 0; }
    return (angle < 0) ? angle + 360 : angle;
}

function drawLinesToAxes(xPos,yPos)
{
	$("#vertline").css('left',xPos);
	$("#horizline").css('top',yPos);
	
	if (xPos > 410 || xPos< 50)
		$("#vertline").css('display', 'none');
	else
		$("#vertline").css('display', 'block');
		
	if (yPos > 400 || yPos< 20)
		$("#horizline").css('display', 'none');
	else
		$("#horizline").css('display', 'block');
}






