$(document).ready()
{
	
	$(function() {
		
		
		$(document).mousemove(function(ev){
			if(!ev)ev=window.event;
			drawLinesToAxes(ev.clientX,ev.clientY);
			return false;
		});
	});
	
};

function toggleCheckbox(num)
{
	var loc;
	var cb1 = document.getElementById('sample1_age');
	var cb2 = document.getElementById('sample2_age');
	
	if (cb1.checked && cb2.checked)
		loc = "images/chart_both.png"
	else if (cb1.checked && !cb2.checked)
		loc = "images/chart_sample1.png"
	else if (!cb1.checked && cb2.checked)
		loc = "images/chart_sample2.png"
	else 
		loc = "images/chart.png"
		
	$('#graphIm').attr("src",loc);
}


function drawLinesToAxes(xPos, yPos)
{
	yPos = yPos + $("#chart").scrollTop(); //allow for the fact that the diagram is in a div that might be scrolling on small screens
	$("#horizline").css('top',yPos);

	if (yPos > $("#chart").height() || xPos > $("#chart").width()+30)
		$("#horizline").css('display', 'none');
	else
		$("#horizline").css('display', 'block');
}

function reveal (sampleNum)
{
	$(("#ageSample")+sampleNum).show();
}






